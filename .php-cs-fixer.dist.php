<?php

$header = <<<EOF
This file is part of the guanguans/laravel-exception-notify.

(c) guanguans <ityaozm@gmail.com>

This source file is subject to the MIT license that is bundled.
EOF;

return require __DIR__.'/.php_cs.dist';
